import os, sys
import subprocess
import time
from shutil import copyfile

def execute(command:str):     
    p = subprocess.Popen(args=command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    return output.decode("utf-8").splitlines()

if __name__ == "__main__" : 
    install_dev_name = sys.argv[1]
    print(f"2. clean {install_dev_name} formated...")
    lines = execute("lvdisplay -v | grep 'LV Path' | awk '{print $3}' ")
    for path in lines:
        execute(f"echo y | lvremove {path}")

    execute(f"echo d | fdisk {install_dev_name}")
    print("")
    execute(f"echo w | fdisk {install_dev_name}")
    print("")