import os, sys, re

if __name__ == "__main__" : 
    hostname_path="/root/hostname"
    hostname = ""
    if os.path.exists(hostname_path):
        os.remove(hostname_path)
        
    while not hostname:
        try:
            hostname = input('Please enter hostname:')
            if hostname:
                find_some_error_match = re.search("[\s+\.\!\/_,$%^*(+\"\']+|[+——！，。？、~@#￥%……&*（）]+", hostname,  flags=0)
                if find_some_error_match:
                    hostname = ""
                    raise Exception("find some special character in hostname...")
                if 4 > len(hostname) or len(hostname) > 12:
                    hostname = ""
                    raise Exception("hostname length must be in 4 ~ 12 ...")

                with open(hostname_path, "w") as f:
                    f.write(hostname)
            else:
                if input("Are you sure use default hostname ???(y/n)") == "y":
                    break
        except Exception as e:
            print(e)

