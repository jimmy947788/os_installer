import os
import subprocess
import time
from shutil import copyfile

def execute(command:str):     
    p = subprocess.Popen(args=command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    return output.decode("utf-8").splitlines()

if __name__ == "__main__" : 
    execute(f"echo '' > /root/install_device_name")
    lines = execute("fdisk -l | grep 'Disk /dev/sd' | awk '{print $2 $3}'")
    install_dev_name = None
    print("")
    print("1.list all storage in this rig.")
    print("===============================================================")
    for line in lines:
        dev_name = line.split(":")[0]
        dev_size = float(line.split(":")[1])
        print(f"\tdev name:{dev_name}, size:{dev_size}GB" )
        if dev_size >= 50:
            install_dev_name = dev_name
    print("===============================================================")

    if install_dev_name:
        #print(f"found msata device name :{install_dev_name}")
        execute(f"echo '{install_dev_name}' > /root/install_device_name")
        print(f"will be install Richash OS to {install_dev_name}")
    else:
        execute(f"echo '' > /root/install_device_name")
        print("not found msata to install Richash OS... ")
    print("")
    