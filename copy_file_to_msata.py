import os, sys
import subprocess
import time
from shutil import copyfile

def execute(command:str):     
    p = subprocess.Popen(args=command, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    return output.splitlines()

if __name__ == "__main__" : 

    lines = execute("vgdisplay -s")
    for line in lines:
        decode_line = line.decode("utf-8").split(" ")
        vgname = decode_line[2].replace('"', "")
        vgsize = float(decode_line[3].replace("<", ""))
        if vgsize >= 50:
            print(f"target disk vgname:{vgname}, vgsize:{vgsize}")
            time.sleep(1)
            print(f"mount /dev/{vgname}/root to /mnt/RichashOS/....")
            execute(f"mount -t ext4 /dev/{vgname}/root /mnt/RichashOS/")
            
            """
            time.sleep(1)
            source = "/root/hostname"
            destination = "/mnt/RichashOS/var/richash/hostname"
            if os.path.exists(destination):
                os.remove(destination)
            if os.path.exists(source):
                print("copy hostname to RichashOS")
                copyfile(source, destination)
            """
            
            time.sleep(1)
            source = "/root/system_config.json"
            destination = "/mnt/RichashOS/var/richash/system_config.json"
            print("copy system_config.json to RichashOS")
            if os.path.exists(destination):
                os.remove(destination)
            copyfile(source, destination)

            time.sleep(1)
            source = "/root/miner_config.json"
            destination = "/mnt/RichashOS/var/richash/miner_config.json"
            print("copy miner_config.json to RichashOS")
            if os.path.exists(destination):
                os.remove(destination)
            copyfile(source, destination)
            
            execute("umount /mnt/RichashOS")